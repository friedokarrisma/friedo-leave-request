<?php
class Menu_model extends Ci_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get()
	{
		$get = $this->db->get('menu');
		
		return $get->result();
	}
	
	public function update($id, $data)
	{
		$this->db->where('menu_id', $id);
		$this->db->update('menu', $data);
	}
}

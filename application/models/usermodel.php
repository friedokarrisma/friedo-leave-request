<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Usermodel extends CI_Model
{
   	public function get_menu_for_level($user_level)
   	{
      $this->db->from('menu');
      $this->db->like('menu_allowed','+'.$user_level.'+');
      $result = $this->db->get();
      return $result;
   	}

   	function get_array_menu($id)
	{
		$this->db->select('menu_allowed');
		$this->db->from('menu');
		$this->db->where('menu_id',$id);
		$data = $this->db->get();
		if($data->num_rows() > 0)
	   	{
			$row = $data->row();
			$level = $row->menu_allowed;
			$arr = explode('+',$level);
			return $arr;
		}
		else
		{
			die();
		}
	}
	function get_all_user()
	{
	   $this->db->from('user');
	   $this->db->join('level','level_id=user_level','left');
	   return $this->db->get();
	}
	function get_all_level()
	{
	   return $this->db->get('level');
	}
	function insert_data_user($data)
	{
	   $this->db->insert('user',$data);
	}
	function get_user_by_id($id)
	{
	   $this->db->where('user_id',$id);
	   return $this->db->get('user');
	}
	function get_user_by_level($level){
		$this->db->where('user_level',$level);
	   	return $this->db->get('user');	
	}
	function update_data_user($data,$id)
	{
	   $this->db->where('user_id',$id);
	   $this->db->update('user',$data);
	}
	function delete_user($id)
	{
	   $this->db->where('user_id',$id);
	   $this->db->delete('user');
	}

	// sample
	/*
	* Insert data to the content table
	*/
	public function insertDataToDB($id,$data) {
	    $this->db->where('id',$id); 
	    $this->db->update('cuti', $data);
	}

	public function get_data(){
		$this->db->where('user_id',1);
	   	return $this->db->get('user');
	}

	/*
	* Get the Inserted data from content table
	*/
	public function getLastEnrtyData() {
	    $this->db->select('*');
	    $this->db->where('id',1);
	    $this->db->from('content');
	    //$last_id = $this->db->insert_id();
	    //$this->db->where('id', $last_id);

	    return $this->db->get();
	}
}
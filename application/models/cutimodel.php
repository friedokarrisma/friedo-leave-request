<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Cutimodel extends CI_Model
{
	function get_all_level(){
	   	return $this->db->get('level');
	}
	function insert_data_cuti($data){
	   	$this->db->insert('cuti',$data);
	}
	// function get_user_id(){
	// 	$this->db->select('user_id');
	// 	$this->db->from('user');
	// 	$query = $this->db->get();
	// 	if ($query->num_rows() > 0) {
	//         return $query->row()->user_id;
	//     }
	//     return false;
	// }
	function daftar_cuti($id){
		$this->db->select('*');
		$this->db->from('cuti');
		$this->db->where('user_id',$id);
		return $this->db->get();
	}
	function daftar_approval($uname){
		$this->db->select('cuti.*, user.user_nama');
		$this->db->from('cuti');
		$this->db->join('user','user.user_id = cuti.user_id','left');
		$this->db->where('pengalihan',$uname);
		return $this->db->get();	
	}
	function atasan($id){
		$this->db->select('atasan')->from('user')->where('user_id',$id);
		return $this->db->get();
	}
	function get_id_cuti($id){
		//$this->db->select('id,status')->from('cuti')->where('id', $id);
		$this->db->select('*')->from('cuti')->where('id', $id);
		return $this->db->get();
	}
	function approval($data, $id){
		$this->db->where('id',$id);
	   	$this->db->update('cuti',$data);
	}
	function get_user_id_from_cuti ($nama){ //get user_id from cuti
		$this->db->select('user_id');
		$this->db->from('cuti');
		$this->db->where('pengalihan',$nama);
		return $this->db->get()->row()->user_id;
	}
	function get_ct_by_uid ($uid){
		$this->db->select('cuti_tahunan');
		$this->db->from('user');
		$this->db->where('user_id',$uid);
		return $this->db->get()->row()->cuti_tahunan;
	}
	function get_jc_from_cuti ($nama){
		$this->db->select('jumlah_cuti');
		$this->db->from('cuti');
		$this->db->where('pengalihan',$nama);
		return $this->db->get()->row()->jumlah_cuti;	
	}

	function get_cuti_tahunan_by_id($id){//get user_id dari cuti
		// $this->db->select('*');
		// $this->db->from('user');
		// $this->db->where('user_id',$id);
		// $query = $this->db->get();
		// print_r($query->row());
		// exit;
		// return $query->row()->cuti_tahunan;
	}
	function get_cuti_ambil($id){
		// $this->db->select('jumlah_cuti')->from('cuti')->where('user_id',$id);
		// return $this->db->get()->row();	
	}
	function min_cuti($data,$id){
		$this->db->where('user_id',$id);
		$this->db->set('cuti_tahunan',$data);
	   	$this->db->update('user');	
	}
}
?>
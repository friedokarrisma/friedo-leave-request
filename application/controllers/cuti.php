<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Cuti extends CI_Controller
{
   	public function __construct(){
      	parent::__construct();
   	}
   	public function insert_cuti(){
		// mencegah user yang belum login untuk mengakses halaman ini
		$this->auth->restrict();
		// mencegah user mengakses menu yang tidak boleh ia buka (manejemen user menu_id = 2)
		$this->auth->cek_menu(6);

		$this->load->library('form_validation');

		
		$this->form_validation->set_rules('tgl_mulai', 'tgl mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'tgl selesai', 'trim|required');
		//$this->form_validation->set_rules('jumlah_cuti', 'jumlah_cuti', 'trim|required|matches[password]');
		$this->form_validation->set_rules('jumlah_cuti', 'jumlah cuti', 'trim');
		$this->form_validation->set_rules('alasan', 'alasan', 'trim|required');
		$this->form_validation->set_rules('contact', 'contact', 'trim|required');
		$this->form_validation->set_rules('pengalihan', 'pengalihan', 'trim|required');
		$this->form_validation->set_rules('atasan', 'atasan', 'trim|required');
		
		$this->form_validation->set_error_delimiters(' <span style="color:#FF0000">', '</span>');

		if ($this->form_validation->run() == FALSE){
			$level 		= $this->session->userdata('level');
			//$levelUp 	= $level - '1'; //harus di ganti function ini, jika sudah menggunakan multilevel
			$data['level'] 	= $this->cutimodel->get_all_level();
			$data['user'] 	= $this->usermodel->get_user_by_level($level);
			$data['atasan'] = $this->cutimodel->atasan($this->session->userdata('user_id'));
			$this->template->set('title','Tambah Cuti Baru | MyWebApplication.com');
			$this->template->load('template','admin/form_insert_cuti',$data);
		}
		else{
			$uid = $this->session->userdata('user_id');
			$data_user = array(
			'user_id' 		=>$uid,
			'tgl_mulai'   	=>$this->input->post('tgl_mulai'),
			'tgl_selesai'   =>$this->input->post('tgl_selesai'),
			'jumlah_cuti'   =>$this->input->post('jumlah_cuti'),
			'alasan'		=>$this->input->post('alasan'),
			'contact'		=>$this->input->post('contact'),
			'pengalihan'   	=>$this->input->post('pengalihan'),
			'atasan'   		=>$this->input->post('atasan'),
			'status'		=>'0'
			);
			print $uid;
			$this->cutimodel->insert_data_cuti($data_user);
			// kembalikan ke halaman manajemen user
			redirect('cuti/daftar_cuti');    
		}
   	}
   	public function daftar_cuti(){
		$this->auth->restrict();
		$this->auth->cek_menu(6);
		$id = $this->session->userdata('user_id');
		$data['cutiku'] = $this->cutimodel->daftar_cuti($id);
		$this->template->set('title','Daftar Cuti | MyWebApplication.com');
		$this->template->load('template','admin/daftar_cuti',$data);
   	}
   	public function daftar_approval(){
   		$this->auth->restrict();
		$this->auth->cek_menu(6);
		$id = $this->session->userdata('nama');
		$data['cuti_orang'] = $this->cutimodel->daftar_approval($id);
		$this->template->set('title','Daftar Approval | MyWebApplication.com');
		$this->template->load('template','admin/daftar_approval',$data);
   	}
   	public function approval(){
   		$this->auth->restrict();
		$this->auth->cek_menu(6);
		$this->load->library('form_validation');

		$id   = $this->uri->segment(3);
		$nama = $this->session->userdata('nama');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
            $data['cuti']  = $this->cutimodel->get_id_cuti($id);
            $this->template->set('title', 'Edit Cuti | MyWebApplication.com');
            $this->template->load('template', 'admin/form_edit_cuti', $data);
        } else {
        	$uid = $this->cutimodel->get_user_id_from_cuti($nama);
        	$ct  = $this->cutimodel->get_ct_by_uid($uid);
        	$jc  = $this->cutimodel->get_jc_from_cuti($nama);
        	$sc  = $ct - $jc;
        	if ($sc > 0){
	        	$this->cutimodel->min_cuti($sc, $uid);
	            $data_user = array(
	                'status' => $this->input->post('status')
	            );
	            $this->cutimodel->approval($data_user,$id);
	            redirect('cuti/daftar_approval');
        	}else{
        		echo '<script language="javascript">';
				echo 'alert("Cuti tahunan telah habis.");';
				echo 'window.location.href="../daftar_approval";';
				echo '</script>';

        	}
        }
   	}
}
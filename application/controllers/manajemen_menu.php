<?php

class Manajemen_menu extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->auth->restrict();
		$this->auth->cek_menu(6);
		
		$this->load->model('menu_model');
		
		if ($_POST)
		{
			$arr_menu = $this->menu_model->get();
			
			foreach ($arr_menu as $menu)
			{
				$key = $menu->menu_id;
				
				if (!isset($_POST['menu'][$key]))
				{
					$user = array();
				}
				else
				{
					$user = $_POST['menu'][$key];
				}
				
				if (!in_array(1, $user))
				{
					$user[] = 1;
				}
				
				$user_list = array('menu_allowed' => '+'.implode('+', $user).'+');
				
				$this->menu_model->update($key, $user_list);
			}
		}
		
		$data['arr_menu'] = $this->menu_model->get();

		$this->template->set('title', 'Manajemen Menu');
		$this->template->load('template','admin/manajemen_menu',$data);
	}
	
	public function insert()
	{
		
	}
	
	public function edit()
	{
		
	}
}

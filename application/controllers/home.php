<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        if ($this->auth->is_logged_in() == false) {
            $this->login();
        } else {
            $nama = $this->session->userdata('nama');
            $data['cuti_orang'] = $this->cutimodel->daftar_approval($nama);
            $id = $this->session->userdata('user_id');
            $data['cutiku'] = $this->cutimodel->daftar_cuti($id);
            $this->template->set('title', 'Welcome user! | MyWebApplication.com');
            $this->template->load('template', 'admin/index', $data);
        }
    }
    public function login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_error_delimiters(' <span style="color:#FF0000">', '</span>');
        
        if ($this->form_validation->run() == FALSE) {
            $this->template->set('title', 'Login Form | MyWebApplication.com');
            $this->template->load('template', 'admin/login_form');
        } else {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $success  = $this->auth->do_login($username, $password);
            if ($success) {
                // lemparkan ke halaman index user
                redirect('home/index');
            } else {
                $this->template->set('title', 'Login Form | MyWebApplication.com');
                $data['login_info'] = "Maaf, username dan password salah!";
                $this->template->load('template', 'admin/login_form', $data);
            }
        }
    }
    
    function manajemen_user()
    {
        $this->auth->restrict();
        $this->auth->cek_menu(2);
        $data['all_user'] = $this->usermodel->get_all_user();
        $this->template->set('title', 'Manajemen User | MyWebApplication.com');
        // load view tabel_user.php di folder application/views/admin/
        $this->template->load('template', 'admin/daftar_user', $data);
    }
    function insert_user()
    {
        $this->auth->restrict();
        $this->auth->cek_menu(2);
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('password_conf', 'Password Confirmation', 'trim|required|matches[password]');
        $this->form_validation->set_rules('level', 'Level', 'trim|required');
        
        $this->form_validation->set_error_delimiters(' <span style="color:#FF0000">', '</span>');
        
        if ($this->form_validation->run() == FALSE) {
            $data['level'] = $this->usermodel->get_all_level();
            $this->template->set('title', 'Tambah User Baru | MyWebApplication.com');
            $this->template->load('template', 'admin/form_input_user', $data);
        } else {
            $data_user = array(
                'user_nama' => $this->input->post('nama'),
                'user_username' => $this->input->post('username'),
                'user_password' => md5($this->input->post('password')),
                'user_level' => $this->input->post('level')
            );
            $this->usermodel->insert_data_user($data_user);
            // kembalikan ke halaman manajemen user
            redirect('home/manajemen_user');
        }
    }
    function edit_user()
    {
        $this->auth->restrict();
        $this->auth->cek_menu(2);
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('password_conf', 'Password Confirmation', 'trim|required|matches[password]');
        $this->form_validation->set_rules('level', 'Level', 'trim|required');
        
        $this->form_validation->set_error_delimiters(' <span style="color:#FF0000">', '</span>');
        
        // dapatkan id user dari segment ke-3 dari URI
        $id = $this->uri->segment(3);
        
        if ($this->form_validation->run() == FALSE) {
            $data['level'] = $this->usermodel->get_all_level();
            // dapatkan data user yg akan diedit dari database
            $data['user']  = $this->usermodel->get_user_by_id($id);
            $this->template->set('title', 'Edit User | MyWebApplication.com');
            $this->template->load('template', 'admin/form_edit_user', $data);
        } else {
            $data_user = array(
                'user_nama' => $this->input->post('nama'),
                'user_username' => $this->input->post('username'),
                'user_password' => md5($this->input->post('password')),
                'user_level' => $this->input->post('level')
            );
            $this->usermodel->update_data_user($data_user, $id);
            // kembalikan ke halaman manajemen user
            redirect('home/manajemen_user');
        }
    }
    function delete_user()
    {
        $this->auth->restrict();
        $this->auth->cek_menu(2);
        $id = $this->uri->segment(3);
        $this->usermodel->delete_user($id);
        // kembalikan ke halaman manajemen user
        redirect('home/manajemen_user');
    }
    function logout()
    {
        if ($this->auth->is_logged_in() == true) {
            // jika dia memang sudah login, destroy session
            $this->auth->do_logout();
        }
        // larikan ke halaman utama
        redirect('home');
    }

    // sample
    /*
    * this is just to load you view page call test.php from your view folder (Which is my given view file)
    */ 

    public function test() {
        // $this->auth->restrict();
        // $this->auth->cek_menu(6);
        // $data['coba'] = $this->usermodel->getLastEnrtyData();
        // $this->template->set('title', 'Sample | MyWebApplication.com');
        // $this->template->load('template', 'admin/sample_view', $data);
        $this->auth->restrict();
        $this->auth->cek_menu(6);
        $id = $this->session->userdata('nama');
        $data['cuti_orang'] = $this->cutimodel->daftar_approval($id);
        $this->template->set('title','Daftar Approval | MyWebApplication.com');
        $this->template->load('template','admin/sample_view',$data);
    }

     /*
     * this is for insert data via Ajax and get the data
     */

    public function insertByajax() {
        $this->auth->restrict();
        $this->auth->cek_menu(6);
        $data['status'] = $this->input->post('status');
        $id = $this->input->post('id');
        print_r ($data);
        $insert = $this->usermodel->insertDataToDB($id,$data);
    }
}
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 
<div id="main-content">
   <div class='title'>Form Input User</div>
    
   <?php echo form_open('home/insert_user');?>
   <table width='100%'>
      <tr>
         <td>Nama</td>
         <td>:</td>
         <td>
            <?php echo form_input('nama',set_value('nama'));?>
            <?php echo form_error('nama');?>
         </td>
      </tr>
      <tr>
         <td>Username</td>
         <td>:</td>
         <td>
            <?php echo form_input('username',set_value('username'));?>
            <?php echo form_error('username');?>
         </td>
      </tr>
      <tr>
         <td>Password</td>
         <td>:</td>
         <td>
            <?php echo form_password('password');?>
            <?php echo form_error('password');?>
         </td>
      </tr>
      <tr>
         <td>Konfirmasi Password</td>
         <td>:</td>
         <td>
            <?php echo form_password('password_conf');?>
            <?php echo form_error('password_conf');?>
         </td>
      </tr>
      <tr>
         <td>Level</td>
         <td>:</td>
         <td>
            <?php
               // menampilkan dropdown level
               foreach($level->result() as $row)
               {
                  $array_level[$row->level_id] = $row->level_nama;
               }
               echo form_dropdown('level',$array_level,set_value('level'));
            ?>
            <?php echo form_error('level');?>
         </td> 
      </tr>
      <tr>
         <td></td>
         <td></td>
         <td><?php echo form_submit('submit','Simpan');?></td>
      </tr>
   </table>
   <?php echo form_close();?>
</div>
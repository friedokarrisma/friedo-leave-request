<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 
<div id="main-content">
   	<ul class="dropdown">
      <?php
        function print_recursive_list($data)
        {
            $str = "";
            foreach($data as $list)
            {
                $str .= "<li><a href='javascript:void(0)'>".$list['nama']."</a>";
                $subchild = print_recursive_list($list['child']);
                if($subchild != '') $str .= "<ul>".$subchild."</ul>";
                $str .= "</li>";
            }
            return $str;
        }

        foreach($multilevel as $data)
        {
          echo '<li><a href="#">'.$data['nama'].'</a>';
          echo '<ul class="sub_menu">';
          echo print_recursive_list($data['child']);
          echo '</ul>';
        }
      ?>
    </ul>
</div>	
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <style type="text/css">
	.table{
		font-size: 13px;	
	}
</style>
<?php
        echo form_open('manajemen_menu');
    ?>
<div id="main-content">
	<h3>Manajemen Menu</h3>  
	<table cellpadding="10" class='table'>
		<thead>
			<tr>
				<th>Menu</th>
				<th><center>Root</center></th>
				<th><center>BOD</center></th>
				<th><center>Manager</center></th>
				<th><center>Dept Head</center></th>
				<th><center>Section Head</center></th>
				<th><center>Tech Consultant</center></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($arr_menu as $menu): ?>
		<?php
			$arr_user = explode('+', $menu->menu_allowed);
			
			$level = array('1'=>'', '2'=>'', '3'=>'', '4'=>'', '5'=>'','6'=>'');
			
			foreach ($arr_user as $user)
			{
				$level[$user] = 'checked';
			}
		?>
			<tr>
				<td><?= $menu->menu_nama; ?></td>
				<td>
					<center><input disabled="disabled" type="checkbox" name="menu[<?= $menu->menu_id; ?>][]" value="1" <?= $level[1]; ?>/></center>
				</td>
				<td>
					<center><input type="checkbox" name="menu[<?= $menu->menu_id; ?>][]" value="2" <?= $level[2]; ?>/></center>
				</td>
				<td>
					<center><input type="checkbox" name="menu[<?= $menu->menu_id; ?>][]" value="3" <?= $level[3]; ?>/></center>
				</td>
				<td>
					<center><input type="checkbox" name="menu[<?= $menu->menu_id; ?>][]" value="4" <?= $level[4]; ?>/></center>
				</td>
				<td>
					<center><input type="checkbox" name="menu[<?= $menu->menu_id; ?>][]" value="5" <?= $level[5]; ?>/></center>
				</td>
				<td>
					<center><input type="checkbox" name="menu[<?= $menu->menu_id; ?>][]" value="6" <?= $level[6]; ?>/></center>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="7">
					<button class="btn btn-primary">Save</button>
				</td>
			</tr>
		</tfoot>
	</table>
</div>
</form>

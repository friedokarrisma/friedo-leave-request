<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 
    <div id="main-content">
       	<h3>Daftar Approval</h3>
       	<table class='table' width='100%'>
          	<tr>
                <th width='15%' align='left'>Nama</th>
             	<th width='15%' align='left'>Mulai</th>
             	<th width='15%' align='left'>Selesai</th>
             	<th width='15%' align='left'>Pengalihan</th>
             	<th width='15%' align='left'>Atasan</th>
             	<th width='20%' align='center'>Aksi</th>
          	</tr>
       	<?php
       		foreach($cuti_orang->result() as $row)
       		{
      	?>
      	<tr>
            <td><?php echo $row->user_nama;?></td>
            <td><?php echo $row->tgl_mulai;?></td>
    	      <td><?php echo $row->tgl_selesai;?></td>
    	      <td><?php echo $row->pengalihan;?></td>
    	      <td><?php echo $row->atasan;?></td>
          	<td>
              	<?php 
                  $id = $row->id;
                  $userNamaPengalihan = $this->session->userdata('nama');
                  if($userNamaPengalihan == $row->pengalihan){
                    $status = '1';   
                  }else{
                    $status = '2';
                  }
                  
                ?>
                <!--input type="hidden" name="id" id="id" value="<?php echo $id ?>"></input-->
                <!--input type="hidden" readonly="true" class="status" value="<?php echo $status ?>"></input> -->
                <?php if($row->status == 0){ ?>
                  <button type="button" class="btn btn-primary" id="submit">Approve</button>
                <?php }else{ ?>
                  Approved by you
                <?php } ?>
           	</td>
        </tr>
        <?php
          echo $row->status;
       		}
        ?>
       	</table>
    </div>

<!--script type="text/javascript">
    $(document).ready(function() {
        var idx = '';
        $('#submit').click(function() {
            //var idx = $(this).closest('form').find('input[id=id]').val(); //used in the "data" attribute of the ajax call
            var idx = <?php echo $id ?>;
            var stt = <?php echo $status ?>; //used in the "data" attribute of the ajax call
            alert("idx: "+idx);
            alert("stt: "+stt);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/home/insertByajax",
                data: {id:idx,status:stt},
                success: function(message) {
                    // return success message to the id='result' position
                    console.log(JSON.stringify(message));
                    $("#result").html(message);
                    document.location.reload();
                    var idx = '';
                }
            });
        });

    });
    //$(this).closest("form").serialize();
</script-->
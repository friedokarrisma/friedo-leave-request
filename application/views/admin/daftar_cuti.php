<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 
<div id="main-content">
   	<h3>List Cuti</h3>
   	<table class='table' width='100%'>
      	<tr>
         	<th width='15%' align='left'>Mulai Cuti</th>
         	<th width='15%' align='left'>Selesai Cuti</th>
         	<th width='15%' align='left'>Pengalihan</th>
         	<th width='15%' align='left'>Atasan</th>
         	<th width='20%' align='left'>Status</th>
         	<th width='20%' align='center'>Aksi</th>
      	</tr>
   	<?php
   		foreach($cutiku->result() as $row)
   		{
  	?>
  	<tr>
     	<td><?php echo $row->tgl_mulai;?></td>
		<td><?php echo $row->tgl_selesai;?></td>
		<td><?php echo $row->pengalihan;?></td>
		<td><?php echo $row->atasan;?></td>
		<td>
			<?php 
				if($row->status == '0'){
					echo "<font style='color:red;''>Pengajuan Pengalihan</font>";	
				}elseif ($row->status == '1') {
					echo "Pengalihan Diterima | Pengajuan Approve Atasan";
				}elseif ($row->status == '2'){
					echo "Cuti Diterima";
				}else {
					echo "Cuti Ditolak";
				}
			?>
		</td>
        <td>
        	<?php
	            echo anchor('home/edit_user/'.$row->user_id,'Edit');
	            echo ' - ';
	            echo anchor('home/delete_user/'.$row->user_id,'Hapus');
            ?>
     	</td>
    </tr>
    <?php
   		}
   	?>
   	</table>
</div>	
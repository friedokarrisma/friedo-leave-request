<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<style type="text/css">
    body {
        background-color: #eee;
    }

    .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin .checkbox {
        font-weight: normal;
    }
    .form-signin .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
         -moz-box-sizing: border-box;
              box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
    }
    .form-signin .form-control:focus {
        z-index: 2;
    }
    .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }
    .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
</style> 
<div class="container">
    <?php 
        $attributes = array('class' => 'form-signin');
        echo form_open('home/login',$attributes);
    ?>

    <?php
    // hanya untuk menampilkan informasi saja
    if(isset($login_info))
    {
        echo "<span style='background-color:#eee;padding:3px;'>";
        echo $login_info;
        echo '</span>';
    }
    ?>
    <h2 class="form-signin-heading">Please sign in</h2>
    <?php 
        $data = array(
                'name'        => 'username',
                'id'          => 'username',
                'maxlength'   => '100',
                'class'       => 'form-control',
                'placeholder' => 'Username'
            );
        echo form_input($data);
        echo form_error('username'); 
    ?>
    <label class="sr-only">Password</label>
    <?php
        $data = array(
                'name'        => 'password',
                'id'          => 'password',
                'maxlength'   => '100',
                'class'       => 'form-control',
                'placeholder' => 'Password'
            );
        echo form_password($data);
        echo form_error('password');
    ?>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="remember-me"> Remember me
        </label>
    </div>
    <?php 
        $data = array(
                'name'        => 'submit',
                'id'          => 'submit',
                'maxlength'   => '100',
                'class'       => 'btn btn-primary btn-block',
                'value'       => 'Sign In'
            );
        echo form_submit($data);
    ?>
</div>
<?php echo form_close();
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 
<div id="main-content" style="margin: 0px 20px 0px 0px;">
    <h3>Form Input Cuti</h3>
    <?php
        echo form_open('cuti/insert_cuti');
    ?>
    <div class="form-group">
        <input type='hidden' id='id'/>
        <input type="text" id="from" class="form-control" placeholder="Tanggal Mulai Cuti" name="tgl_mulai">
        <?php echo form_error('tgl_mulai');?>
    </div>
    <div class="form-group">
        <input type="text" id="to" name="tgl_selesai" class="form-control" placeholder="Tanggal Selesai Cuti" onchange="fillIt()">
        <?php echo form_error('tgl_selesai');?>
    </div>
    <div class="form-group">
        <input type='text' name='jumlah_cuti' class="form-control" id='jumlah_cuti' readonly="true" placeholder="Jumlah Cuti">
        <?php echo form_error('jumlah_cuti');?>
    </div>
    <div class="form-group">
        <textarea name="alasan" id="alasan" class="form-control" rows="3" placeholder="Alasan Cuti"></textarea>
        <?php echo form_error('alasan');?>
    </div>
    <div class="form-group">
        <textarea name="contact" id="contact" class="form-control" rows="3" placeholder="Kontak Yang Bisa Dihubungi"></textarea>    
        <?php echo form_error('contact');?>
    </div>
    <div class="form-group">
        <label>Pengalihan</label>
        <?php
            // menampilkan dropdown level
            foreach($user->result() as $row){
                if(!($row->user_id != $this->session->userdata('user_id'))){
                    $array_user[$row->atasan] = $row->atasan;        
                }
                else{
                    $array_user[$row->user_nama] = $row->user_nama;        
                }
            }
            if (!$array_user){
                echo form_dropdown('atasan','-----','','class="form-control"');    
            }else{
                echo form_dropdown('pengalihan',$array_user,'','class="form-control"');
            }
        ?>
        <?php echo form_error('pengalihan');?>
    </div>
    <div class="form-group">
        <label>Atasan</label>
        <?php
            foreach ($atasan->result() as $row) {
                # code...
            ?>
                <input type='text' name='atasan' value='<?php echo $row->atasan; ?>' class="form-control" readonly="true">
            <?php
            }
        ?>
        <?php echo form_error('atasan');?>
    </div>
    <div class="form-group">
        <?php echo form_submit('submit','Simpan','class="btn btn-block btn-primary"');?>
    </div>
    <?php echo form_close();?>
</div>

<script type='text/javascript'>
    function workingDaysBetweenDates(startDate, endDate) {
  
        // Validate input
        if (endDate < startDate)
            return 0;
        
        // Calculate days between dates
        var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
        startDate.setHours(0,0,0,1);  // Start just after midnight
        endDate.setHours(23,59,59,999);  // End just before midnight
        var diff = endDate - startDate;  // Milliseconds between datetime objects    
        var days = Math.ceil(diff / millisecondsPerDay);
        
        // Subtract two weekend days for every week in between
        var weeks = Math.floor(days / 7);
        days = days - (weeks * 2);

        // Handle special cases
        var startDay = startDate.getDay();
        var endDay = endDate.getDay();
        
        // Remove weekend not previously removed.   
        if (startDay - endDay > 1)         
            days = days - 2;      
        
        // Remove start day if span starts on Sunday but ends before Saturday
        if (startDay == 0 && endDay != 6) {
            days = days - 1;  
        }
                
        // Remove end day if span ends on Saturday but starts after Sunday
        if (endDay == 6 && startDay != 0) {
            days = days - 1;
        }
        
        return days;
    }

    function fillIt() {
        var sd = new Date(document.getElementById('from').value);
        var ed = new Date(document.getElementById('to').value);
        var numberDays = workingDaysBetweenDates(sd, ed);
        document.getElementById('jumlah_cuti').value = numberDays;
    }
</script>
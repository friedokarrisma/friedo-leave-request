<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<style type="text/css">
	#grad {
		background: red; /* For browsers that do not support gradients */
		background: -webkit-linear-gradient(left top, #777, #eee); /* For Safari 5.1 to 6.0 */
		background: -o-linear-gradient(bottom right, #777, #eee); /* For Opera 11.1 to 12.0 */
		background: -moz-linear-gradient(bottom right, #777, #eee); /* For Firefox 3.6 to 15 */
		background: linear-gradient(to bottom right, #777, #eee); /* Standard syntax */
		margin-left: 30%;
		margin-right: 30%;
	}
	.col h1{
		font-size: 90px;
		font-family: cursive;
		text-shadow: 2px 2px #eaeaea;
		padding-bottom: 15px;
	}
	.col{
		text-align: center; 
		border-radius: 10px;
		box-shadow: 4px 4px #eaeaea;
	}
</style> 

<div id="main-content">
	<h3><?php echo $this->session->userdata('nama'); ?> - Dashboard</h3>
	<div class="row">
		<div class="col-md-3">
			<div class="row">
				<div class="col" id="grad">
					<h1>
						<?php echo $this->session->userdata("cuti_tahunan"); ?>
					</h1>
				</div>
			</div>
		</div>
		<div class="col-md-8" style="padding-top: 16px;">
			<table class='table table-bordered' width='100%'>
				<tr>
					<th colspan="4">Permohonan Cuti Anda</th>
				</tr>
		      	<tr>
		         	<th width='15%' align='left'>Mulai</th>
		         	<th width='15%' align='left'>Selesai</th>
		         	<th width='15%' align='left'>Alasan</th>
		         	<th width='15%' align='left'>Status</th>
		      	</tr>
			   	<?php
			   		if ($cutiku->result())
			   		foreach($cutiku->result() as $row)
			   		{
			  	?>
			  	<tr>
			        <td><?php echo $row->tgl_mulai;?></td>
				    <td><?php echo $row->tgl_selesai;?></td>
				    <td><?php echo $row->alasan;?></td>
				    <td><?php echo $row->status;?></td>
			    </tr>
			    <?php
			   		}else{
			   	?>
			   	<tr>
			   		<td colspan="4">Kamu belum mengajukan cuti.</td>
		   		</tr>
		   		<?php } ?>
		   	</table>
		</div>
		<div class="col-md-8" style="padding-top: 16px;">
			<table class='table table-bordered' width='100%'>
				<tr>
					<th colspan="4">Permohonan Approval</th>
				</tr>
		      	<tr>
		            <th width='15%' align='left'>Nama</th>
		         	<th width='15%' align='left'>Mulai</th>
		         	<th width='15%' align='left'>Selesai</th>
		         	<th width='15%' align='left'>Alasan</th>
		      	</tr>
			   	<?php
			   		if ($cuti_orang->result())
			   		foreach($cuti_orang->result() as $row)
			   		{
			  	?>
			  	<tr>
			        <td><?php echo $row->user_nama;?></td>
			        <td><?php echo $row->tgl_mulai;?></td>
				    <td><?php echo $row->tgl_selesai;?></td>
				    <td><?php echo $row->alasan;?></td>
			    </tr>
			    <?php
			   		}else{
			   	?>
			   	<tr>
			   		<td colspan="4">Tidak ada permohonan approval.</td>
		   		</tr>
		   		<?php } ?>
		   	</table>
		</div>
	</div>
</div>
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!-- <div class="modal fade hide modal-creator" id="myModal" style="display: none;" aria-hidden="true">
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Edit Gallery</h3>
    </div>
    <div class="modal-body">
      <?php $row = $cuti->row(); echo form_open('cuti/approval/'.$row->id); ?>
         <table width='100%'>
            <?php echo ($row->id); ?>
            <tr>
               <td>Nama</td>
               <td>:</td>
               <td>
                  <?php echo form_input('status',$row->status);?>
                  <?php echo form_error('status');?>
               </td>
            </tr>
            <tr>
               <td></td>
               <td></td>
               <td><?php echo form_submit('submit','Update');?></td>
            </tr>
         </table>
      <?php echo form_close();?>
    </div>
    <div class="modal-footer">
       <p class="span3 resize">The following images are sized incorrectly. Click to edit</p>
       <a href="javascript:;" class="btn" data-dismiss="modal">Close</a>
       <a href="javascript:;" class="btn btn-primary">Next</a>
    </div>
</div> -->

<div id="main-content">
   <div class='title'>Form Edit User</div>
    <a href="#close" rel="modal:close">Close window</a>
   <?php
   $row = $cuti->row();
   echo form_open('cuti/approval/'.$row->id);
   ?>
   <table width='100%'>
      <?php echo ($row->id); ?>
      <tr>
         <td>Nama</td>
         <td>:</td>
         <td>
            <?php echo form_input('status',$row->status);?>
            <?php echo form_error('status');?>
         </td>
      </tr>
      <tr>
         <td></td>
         <td></td>
         <td><?php echo form_submit('submit','Update');?></td>
      </tr>
   </table>
   <?php echo form_close();?>
</div>
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div id="main-content">
<form action="" method="post" accept-charset="utf-8">
        <!-- <table align="center">

            <tr>
                <td>Message :</td>
                <td>
                    <input type="text" name="id" id="id"></input><br/>
                    <textarea name="message" id="message" placeholder="Write here the message"></textarea>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td id="result"> </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>
                    <button type="button" id="submit">Submit</button>
                </td>
            </tr>

        </table>
 -->
    <h3>Daftar Approval</h3>
        <table class='table' width='100%'>
            <tr>
                <th width='15%' align='left'>Nama</th>
                <th width='15%' align='left'>Mulai</th>
                <th width='15%' align='left'>Selesai</th>
                <th width='15%' align='left'>Pengalihan</th>
                <th width='15%' align='left'>Atasan</th>
                <th width='20%' align='center'>Aksi</th>
            </tr>
            <?php
                foreach($cuti_orang->result() as $row)
                {
            ?>
            <tr>
                <td><?php echo $row->user_nama;?></td>
                <td><?php echo $row->tgl_mulai;?></td>
                <td><?php echo $row->tgl_selesai;?></td>
                <td><?php echo $row->pengalihan;?></td>
                <td><?php echo $row->atasan;?></td>
                <td>
                    <?php
                        if($row->status !='2' and $row->status != '1'){
                            echo anchor('cuti/approval/'.$row->id,'Approve');
                            echo ' - ';
                            echo anchor('home/delete_user/'.$row->id,'Cancel');
                        }else{
                            echo '<font style="color:green">Approved by you</font>';
                        }
                    ?>

                    <?php 
                        $id = $row->id;
                        $status = '2';
                    ?>
                    <input type="hidden" readonly="true" id="id" value="<?php echo $id ?>"></input>
                    <input type="hidden" readonly="true" id="status" value="<?php echo $status ?>"></input>
                    <input type="button" class="btn btn-primary" id="submit" value="Appr"></input>
                </td>
            </tr>
            <?php
                echo $row->status;
                }
            ?>
        </table>
    </form>
<?php
    // foreach($coba->result() as $row){
    //     echo $row->message;
    // }
?>
</div>

<script type="text/javascript">
    $(function() {
        $('#submit').click(function() {
            alert("submit ke klik");
            //get input data as a array
            // var post_data = {
            //     'message': $("#message").val()
            // };
            // alert(JSON.stringify(post_data));
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/home/insertByajax",
                data: {id:$("#id").val(),status: $("#status").val()},
                success: function(message) {
                    // return success message to the id='result' position
                    console.log(JSON.stringify(message));
                    $("#result").html(message);
                    document.location.reload();
                }
            });
            return false;
        });

    });
</script>
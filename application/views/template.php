<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<html>
    <title>
        <?php echo $title;?>
    </title>
    <script src="https://code.jquery.com/jquery-1.12.3.js" integrity="sha256-1XMpEtA4eKXNNpXcJ1pmMPs8JV+nwLdEqwiJeCQEkyc=" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <!-- modal start -->
    <script src="<?php echo base_url('assets/js/jquery.modal.min.js'); ?>"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.modal.css" rel="stylesheet" type="text/css" />

    <!-- css -->
    <link href='//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css' rel='stylesheet' />
    <style type="text/css">
        .footer {
            position: fixed;
            right: 0;
            bottom: 0;
            left: 0;
            padding: 1em;
            background-color: #efefef;
            text-align: left;
        }
        .contentKanan{
            padding:100px;
        }
        #main-content .table,input,label,.btn,.form-control{
            font-size: 12px;
        }
    </style>
    <body>
        <div>
            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#">L. R. A.</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Link</a></li>
                    <?php if($this->session->userdata('nama') != null){ ?>
                        <li> 
                            <?php echo anchor('home/logout','Logout'); ?>
                        </li>
                    <?php
                        }
                        
                    ?>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
            <div>
                <?php
                    // tampilkan sidebar menu HANYA apabila user telah login
                    if($this->auth->is_logged_in() == true):
                    $level = $this->session->userdata('level');
                    // ambil menu dari database sesuai dengan level
                    $this->db->order_by('menu_id', 'asc');
                    $menu = $this->usermodel->get_menu_for_level($level);
                ?>
                <div>
                    <div class="col-sm-3 col-md-2 sidebar" style="border-right: 1px solid #eaeaea;">
                        <ul class="nav nav-sidebar">
                            <?php
                                foreach($menu->result() as $row){
                                    echo '<li>'.anchor($row->menu_uri,$row->menu_nama).'</li>';
                                }
                            ?>
                        </ul>
                    </div>
                    <?php
                        endif;
                    ?>
                    <div class="col-md-10" style="margin-bottom:100px;">
                        <?php
                            // dynamic content loaded here
                            echo $contents;
                        ?>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="footer">
                Leave Request Application | 
                Copyright &copy; 2016 Friedo Karrisma Hadi |
                All Right Reserved
            </div>
        </div>
    </body>
    <script>
        $(function() {
            $("#from").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "mm/dd/yy",
                beforeShowDay: $.datepicker.noWeekends,
                onClose: function(selectedDate) {
                    $("#to").datepicker("option", "minDate", selectedDate);
                }
            });
            $("#to").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "mm/dd/yy",
                beforeShowDay: $.datepicker.noWeekends,
                onClose: function(selectedDate) {
                    $("#from").datepicker("option", "maxDate", selectedDate);
                }
            });
        });
    </script>

</html>